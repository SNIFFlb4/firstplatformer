using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour
{
    [SerializeField] private MainCanvas canvas;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeathZone"))
        {
            GameOver();  
        }
    }
      
    public void GameOver ()
    {
        animator.Play("Death");

        if (gameObject.tag=="Player")
            canvas.GameOver();

        StartCoroutine(DeathCuro());
    }

    private IEnumerator DeathCuro ()
    {
        yield return new WaitForSeconds(0.5f);
        
        Destroy(gameObject);
    }
}
