using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField] private Transform[] points;

    [SerializeField] private GameObject HealthRecovery;
    [SerializeField] private GameObject MiniCoin;
    [SerializeField] private GameObject BigCoin;

    [SerializeField] private float TimeToSpawn;

    private float CurrentTime = 0;

    private int PointNumber=0;
    private int Object = 0;
 
    void Start()
    {
        
    }

    void Update()
    {
        if (CurrentTime>=TimeToSpawn)
        {
            CurrentTime = 0;

            Object = Random.Range(0,100);
            PointNumber = Random.Range(0, points.Length);

            GameObject newObj;
            if (Object >= 90)
                newObj = BigCoin;
            else if (Object >= 90 && Object < 90)
                newObj = MiniCoin;
            else
                newObj = HealthRecovery;

            Instantiate(newObj, points[PointNumber].position, Quaternion.identity);
        }
        CurrentTime += Time.deltaTime;
    }
}
