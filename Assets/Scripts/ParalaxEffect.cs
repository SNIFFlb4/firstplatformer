using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxEffect : MonoBehaviour
{
    [SerializeField] private Transform[] layers;
    [SerializeField] private float[] layerSpeed;

    private int layerCount;

    void Start()
    {
        layerCount = layers.Length;
    }

    void Update()
    {
        for (int i=0; i<layerCount; i++)
        {
            Vector2 newVec = new Vector2(transform.position.x * layerSpeed[i], layers[i].position.y);
            layers[i].position = newVec;
        }
    }   
}
