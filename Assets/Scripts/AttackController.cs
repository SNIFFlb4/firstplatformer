using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AttackTimer))]

public class AttackController : MonoBehaviour
{
    [Header("Sounds")]
    [SerializeField] private AudioSource CloseAttackSound;
    [SerializeField] private AudioSource RangeAttackSound;

    [Header("Close Attack")]
    [SerializeField] private Transform attackPosition;
    [SerializeField] private float attackRange;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private float attackDamage;

    [Header("Range Attack")]

    private Animator anim;
    private AttackTimer timer;

    void Start()
    {
        anim = GetComponent<Animator>();
        timer = GetComponent<AttackTimer>();
    }

    public void Attack (bool closeAttack, bool rangeAttack)
    {
        if (closeAttack && timer.CanAttackClose())
        {
            anim.SetTrigger("Attack");
        }
        if (rangeAttack && timer.CanAttackRange())
        {
            RangeAttackSound.Play();
            anim.SetTrigger("RangeAttack");
        }
    }

    public void CloseAttackOn ()
    {
        //Collider2D[] 
        CloseAttackSound.Play();

        Collider2D[] enemies = Physics2D.OverlapCircleAll(attackPosition.position, attackRange, enemyLayer);
        for (int i=0; i<enemies.Length; i++)
        {
            enemies[i].GetComponent<Health>().TakeDamage(attackDamage);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPosition.position, attackRange);
    }
}
