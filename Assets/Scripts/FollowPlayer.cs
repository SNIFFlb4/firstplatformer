using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform character;

    private Vector3 offset;

    void Start()
    {
        offset = transform.position - character.position;
    }

    void Update()
    {
        transform.position = character.position + offset;
    }
}
