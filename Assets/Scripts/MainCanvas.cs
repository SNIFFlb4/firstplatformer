using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainCanvas : MonoBehaviour
{
    [Header("Panels")]
    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject levelsPanel;
    [SerializeField] private GameObject pauseCanvas;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject winGamePanel;
    [SerializeField] private GameObject roundEndPanel;
    [SerializeField] private GameObject settingsPanel;

    [Header("UI")]
    [SerializeField] private Text ScoreText;

    [Header("Audio")]
    [SerializeField] private AudioSource MenuAudio;
    [SerializeField] private AudioSource LevelAudio;

    private void Start()
    {
        HidePanels();
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            MenuAudio.Play();
            MainMenu();
        }
        else
        {
            LevelAudio.Play();
            pauseCanvas.SetActive(true);
        }
    }

    private void HidePanels ()
    {
        mainMenuPanel.SetActive(false);
        pausePanel.SetActive(false);
        levelsPanel.SetActive(false);
        pauseCanvas.SetActive(false);
        gameOverPanel.SetActive(false);
        winGamePanel.SetActive(false);
        roundEndPanel.SetActive(false);
        settingsPanel.SetActive(false);
    }

    public void MainMenu()
    {
        HidePanels();
        mainMenuPanel.SetActive(true);
    }

    public void BackToGame ()
    {
        HidePanels();
        Time.timeScale = 1;
        pauseCanvas.SetActive(true);
    }

    public void Levels()
    {
        HidePanels();
        levelsPanel.SetActive(true);
    }

    public void NextLevel ()
    {
        Time.timeScale = 1;
        SceneController sceneController = new SceneController();
        sceneController.NextRound();
    }

    public void RestartLevel ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        BackToGame();
    }

    public void PauseGame ()
    {
        HidePanels();
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void GameOver ()
    {
        HidePanels();
        gameOverPanel.SetActive(true);
    }

    public void WinGame ()
    {
        HidePanels();
        winGamePanel.SetActive(true);
    }

    public void EndRound()
    {
        HidePanels();
        roundEndPanel.SetActive(true);
    }

    public void Settings ()
    {
        HidePanels();
        settingsPanel.SetActive(true);
    }

    public void UpdateScore(int score)
    {
        ScoreText.text = score.ToString();
    }

    public void ExitGame ()
    {
        Application.Quit();
    }
}
