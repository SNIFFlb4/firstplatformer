using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(MoveController))] 
[RequireComponent (typeof(AttackController))]
[RequireComponent (typeof(Shooter))]

public class InputController : MonoBehaviour
{
    private MoveController characterMove;
    private AttackController characterAttack;

    private bool CanMove;

    void Start()
    {
        CharacterCanMove();
        characterMove = GetComponent<MoveController>();
        characterAttack = GetComponent<AttackController>();
    }

    void Update()
    {
        if (CanMove)
        {
            float horizontalDirection = Input.GetAxis(GlobalStringVars.HORIZONTAL_AXIS);
            bool isJumpPressed = Input.GetButtonDown(GlobalStringVars.JUMP);

            characterMove.Move(horizontalDirection, isJumpPressed);

            bool closeAttack = Input.GetButtonDown(GlobalStringVars.FIRE_1);
            bool rangeAttack = Input.GetButtonDown(GlobalStringVars.FIRE_2);

            characterAttack.Attack(closeAttack, rangeAttack);
        }

        #region ������ �������
        //horizontal = Input.GetAxis("Horizontal");

        //if (horizontal < 0)
        //    render.flipX = true;
        //else if (horizontal > 0)
        //    render.flipX = false;
        //else
        //    characterMove.Stop();   

        //if (horizontal != 0)
        //    anim.SetBool("isRunning", true);
        //else anim.SetBool("isRunning", false);

        //movement = new Vector2(horizontal, 0).normalized;

        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    anim.SetTrigger("Attack");
        //}

        //if (Input.GetKeyDown(KeyCode.Space) && CheckGround())
        //{
        //    characterMove.Jump();
        //    anim.SetTrigger("Jump");
        //}

        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    anim.SetTrigger("RangeAttack");
        //}
        #endregion


    }

    public void CharacterCanMove ()
    {
        CanMove = true;
    }

    public void CharacterStop ()
    {
        CanMove = false;
    }
}
