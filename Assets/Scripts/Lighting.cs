using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighting : MonoBehaviour
{
    [SerializeField] private Transform target1;
    [SerializeField] private Transform target2;
    [SerializeField] private float TimeToGo;
    [SerializeField] private float PrewarmTime;
    [SerializeField] private bool Rotate;

    private TargetJoint2D targetJoint2d;

    private float currentTime;
    private Transform currentTarget;

    void Start()
    {
        targetJoint2d = GetComponent<TargetJoint2D>();
        currentTime = 0;
        currentTarget = target1;
        targetJoint2d.target = currentTarget.position;
    }

    void Update()
    {
        if (currentTime >= PrewarmTime)
        {
            //currentTime = 0;
            PrewarmTime = 0;
            if (currentTime >= TimeToGo)
            {
                currentTime = 0;
                ChangeTarget();
                targetJoint2d.target = currentTarget.position;
            }
        }
        currentTime += Time.deltaTime;
    }

    private void ChangeTarget()
    {
        if (currentTarget == target1)
            currentTarget = target2;
        else 
            currentTarget = target1;
    }
}
