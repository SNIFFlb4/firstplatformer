using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTimer : MonoBehaviour
{
    [SerializeField] private float timeBetweenCloseAttack;
    [SerializeField] private float timeBetweenRangeAttack;

    private float closeAttackTime;
    private float rangeAttackTime;

    private bool closeAttackIsReady=true;
    private bool rangeAttackIsReady=true;

    void Start()
    {
        closeAttackTime = timeBetweenCloseAttack;
        rangeAttackTime = timeBetweenRangeAttack;
    }

    void Update()
    {
        if (!closeAttackIsReady)
        {
            closeAttackTime -= Time.deltaTime;
            if (closeAttackTime <= 0)
            {
                closeAttackIsReady = true;
                closeAttackTime = timeBetweenCloseAttack;
            }
        }
        if (!rangeAttackIsReady)
        {
            rangeAttackTime -= Time.deltaTime;
            if (rangeAttackTime <= 0)
            {
                rangeAttackIsReady = true;
                rangeAttackTime = timeBetweenRangeAttack;
            }
        }
    }

    public bool CanAttackClose ()
    {
        if (closeAttackIsReady && closeAttackTime > 0)
        {
            closeAttackIsReady = false;
            return true;
        }
        else return false;
    }

    public bool CanAttackRange ()
    {
        if (rangeAttackIsReady && rangeAttackTime > 0)
        {
            rangeAttackIsReady = false;
            return true;
        }
        else return false;
    }
}
