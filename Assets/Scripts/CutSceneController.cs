using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneController : MonoBehaviour
{
    [SerializeField] private InputController PlayerInputController;

    private BoxCollider2D collider;
    private Animator animator;

    private void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerInputController.CharacterStop();
            animator.Play("CutScene");
        }
    }

    public void EndCutScene ()
    {
        PlayerInputController.CharacterCanMove();
        collider.enabled = false;
    }
}
