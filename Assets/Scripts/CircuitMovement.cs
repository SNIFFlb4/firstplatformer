using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitMovement : MonoBehaviour
{
    [SerializeField] private Transform[] targets;
    [SerializeField] private float speed;

    private int currentTarget;

    void Start()
    {
        currentTarget = 0;
    }

    void Update()
    {
        if (transform.position==targets[currentTarget].position)
        {
            if (currentTarget == targets.Length-1)
                currentTarget = 0;
            else 
                currentTarget++;
        }
        transform.position = Vector2.MoveTowards(transform.position, targets[currentTarget].position, speed*Time.deltaTime);
    }
}
