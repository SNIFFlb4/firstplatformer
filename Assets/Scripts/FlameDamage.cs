using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameDamage : MonoBehaviour
{
    [SerializeField] private Transform flamePosition;
    [SerializeField] private float flameRange;
    [SerializeField] private float damage;
    [SerializeField] private LayerMask player;
    [SerializeField] private float timePerDamage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            FlameAttack();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        StopAllCoroutines();
    }

    public void FlameAttack()
    {
        Collider2D[] playerInFlame = Physics2D.OverlapCircleAll(flamePosition.position, flameRange, player);
        if (playerInFlame.Length!=0)
        {
            playerInFlame[0].GetComponent<Health>().TakeDamage(damage);
        }
        StartCoroutine(FlameCuro());

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(flamePosition.position, flameRange);
    }

    private IEnumerator FlameCuro ()
    {
        yield return new WaitForSeconds(timePerDamage);
        FlameAttack();
    }
}
