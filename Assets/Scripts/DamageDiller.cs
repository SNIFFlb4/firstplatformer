using System.Collections;
using UnityEngine;

public class DamageDiller : MonoBehaviour
{
    [SerializeField] private float damage;

    private void Start()
    {
        if (gameObject.activeSelf && gameObject.CompareTag("Bullet"))
            StartCoroutine(DeathCuro());
    }

    //��������� �����
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Damageble") || collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Health>().TakeDamage(damage);
            if (gameObject.CompareTag("Bullet"))
                Destroy(gameObject);
        }
    }

    //������������ ����� 
    private IEnumerator DeathCuro () 
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
}
    