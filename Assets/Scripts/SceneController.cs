using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public MainCanvas canvas;
    
    private int SceneCount; //���-�� ���� � �������
    private int thisScene; //���������� ����� ������� �����

    void Start()
    {
        SceneCount = SceneManager.sceneCountInBuildSettings;
        thisScene = SceneManager.GetActiveScene().buildIndex;
    }

    public void NextRound ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (++thisScene < SceneCount)
            {
                Time.timeScale = 0;
                canvas.EndRound();
            }
            else
                canvas.WinGame();
        }

    }
}
