using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]

public class MoveController : MonoBehaviour
{
    [Header("Sounds")]
    [SerializeField] private AudioSource JumpSound;
    [SerializeField] private AudioSource RunSound;

    [Header("Move Vars")]
    [SerializeField, Range(0, 10)] private float speed;
    [SerializeField, Range(0, 10)] private float jumpForce;

    [Header("Settings")]
    [SerializeField] private Transform groundCollider;
    [SerializeField] private  LayerMask groundLayer;
    [SerializeField] private float jumpOffset;
    [SerializeField] private AnimationCurve curve;

    private int JumpCounter = 0;
    private bool isGrounded=false;
    private float TimeToStep = 0.3f;
    private float StepTimer;

    private Animator animator;
    private SpriteRenderer render;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        render = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        Vector3 circlePosition = groundCollider.position;
        isGrounded = Physics2D.OverlapCircle(circlePosition, jumpOffset, groundLayer);
    }

    public void Move (float direction, bool isJumpPressed)
    {
        if (isJumpPressed)
            Jump();
        if (Mathf.Abs(direction) >= 0.01f)
        {
            if (StepTimer<=0 && isGrounded)
            {
                StepTimer = TimeToStep;
                RunSound.Play();
            }
            StepTimer -= Time.deltaTime;
            FlipSprite(direction);
            HorizontalMovement(direction);
        }
        else
            animator.SetBool("isRunning", false);
    }

    private void FlipSprite (float direction)
    {
        if (direction < 0)
            render.flipX = true;
        else render.flipX = false;
    }

    private void Jump ()
    {
        if (isGrounded)
            JumpCounter=0;

        if (JumpCounter<2)
        {
            JumpSound.Play();
            animator.SetTrigger("Jump");
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
        JumpCounter++;
    }

    private void HorizontalMovement (float direction)
    {
        animator.SetBool("isRunning", true);
        rb.velocity = new Vector2(curve.Evaluate(direction)*speed, rb.velocity.y);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCollider.position, jumpOffset);
    }
}
