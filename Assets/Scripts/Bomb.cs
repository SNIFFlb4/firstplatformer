using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public GameObject bomb;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Damageble"))
        {
            gameObject.SetActive(false);
            bomb.SetActive(true);
            Invoke("HideBomb", 0.5f);

        }
    }

    private void HideBomb ()
    {
        bomb.SetActive(false);
    }
}
