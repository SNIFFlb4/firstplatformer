using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarUI : MonoBehaviour
{
    public Transform healthBar;

    private Vector3 barLocalScale;

    void Start()
    {
        barLocalScale = healthBar.localScale;
    }

    public void UpdateHP (float health, float maxhealth)
    {
        float HealthForBar = health / maxhealth;
        if (HealthForBar <= 0)
            healthBar.gameObject.SetActive(false);
        else
        {
            barLocalScale.x = HealthForBar;
            healthBar.localScale = barLocalScale;
        }
    }
}
