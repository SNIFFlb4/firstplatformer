using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VolumeController : MonoBehaviour
{
    private List<AudioSource> musicAudio = new List<AudioSource>();
    private List<AudioSource> soundsAudio = new List<AudioSource>();
     
    void Start()
    {
        GameObject[] objWithMusic = GameObject.FindGameObjectsWithTag("Music");
        for (int i = 0; i < objWithMusic.Length; i++)
        {
            musicAudio.Add(objWithMusic[i].GetComponent<AudioSource>());
        }

        GameObject[] objWithSounds = GameObject.FindGameObjectsWithTag("Sounds");
        for (int i = 0; i < objWithSounds.Length; i++)
        {
            soundsAudio.Add(objWithSounds[i].GetComponent<AudioSource>());
        }

        SetSoundsVolume(0.3f);
        SetMusicVolume(0.3f);
    }

    public void SetMusicVolume (float volume)
    {
        for (int i = 0; i < musicAudio.Count; i++)
            musicAudio[i].volume = volume;
    }

    public void SetSoundsVolume(float volume)
    {
        for (int i = 0; i < soundsAudio.Count; i++)
            soundsAudio[i].volume = volume;
    }
}
