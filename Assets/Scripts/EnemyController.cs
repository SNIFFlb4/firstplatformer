using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float timeToRevert;

    private Rigidbody2D rigidbody;
    private Animator animator;
    private SpriteRenderer sprite;

    private const float IDLE_STATE = 0;
    private const float WALK_STATE = 1;
    private const float REVERT_STATE = 2;

    private float currentState, currentTimeToRevert;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();

        currentTimeToRevert = 0;
        currentState = WALK_STATE;
    }

    void Update()
    {
        if (currentTimeToRevert>=timeToRevert)
        {
            currentTimeToRevert = 0;
            currentState = REVERT_STATE;
        }

        switch (currentState)
        {
            case IDLE_STATE:
                currentTimeToRevert += Time.deltaTime;
                break;
            case WALK_STATE:
                rigidbody.velocity = Vector2.right * speed;
                break;
            case REVERT_STATE:
                sprite.flipX = !sprite.flipX;
                speed *= -1;
                currentState = WALK_STATE;
                break;  
        }

        animator.SetFloat("Velocity", rigidbody.velocity.magnitude);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GetComponent<Health>().isAlive)
        {
            if (collision.CompareTag("EnemyStopper"))
                currentState = IDLE_STATE;
            if (collision.CompareTag("Player"))
                animator.Play("Attack");
        }
    }
}
