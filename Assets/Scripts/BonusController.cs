using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusController : MonoBehaviour
{
    public GameObject boomParticle; 

    private void OnTriggerEnter(Collider other)
    {
        boomParticle.SetActive(true);
        gameObject.SetActive(false);
    }
}
