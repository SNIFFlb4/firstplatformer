using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletPosition;
    [SerializeField] private float bulletSpeed;

    private SpriteRenderer sprite;
    private SpriteRenderer bulletSprite;
    private float shootDirection;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        bulletSprite = bullet.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (sprite.flipX)
            shootDirection = -1;
        else
            shootDirection = 1;
    }

    public void Shoot ()
    {
        if (shootDirection > 0)
            bulletSprite.flipX = false;
        else
            bulletSprite.flipX = true;


        GameObject currentBullet = Instantiate(bullet, bulletPosition.position, Quaternion.identity);
        Rigidbody2D  currentBulletVelocity = currentBullet.GetComponent<Rigidbody2D>();
        

        MoveBullet(currentBulletVelocity, shootDirection);
    }



    private void MoveBullet (Rigidbody2D currentBulletVelocity, float direction)
    {

        currentBulletVelocity.velocity = new Vector2(bulletSpeed*direction, currentBulletVelocity.velocity.y);
    }
}
