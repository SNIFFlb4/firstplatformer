using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slider : MonoBehaviour
{
    public float timeToChangeDirection;

    private Rigidbody2D rigidbody2D;
    private SliderJoint2D sliderJoint;
    private JointMotor2D motor;

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        sliderJoint = GetComponent<SliderJoint2D>();
        StartCoroutine("ChangeDirection");
    }

    private IEnumerator ChangeDirection ()
    {
        while(true)
        {
            yield return new WaitForSeconds(timeToChangeDirection);
            motor = sliderJoint.motor;
            motor.motorSpeed *= (-1);
            sliderJoint.motor = motor;
        }
    }
}
