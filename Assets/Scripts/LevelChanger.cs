using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    public void SetLevel (int level)
    {
        if (level <= SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene(level);
        if (Time.timeScale == 0)
            Time.timeScale = 1;
    }
}
