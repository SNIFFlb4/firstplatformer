using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    public bool shakeCamera;
    public PlatformEffector2D[] effectors;
    public CinemachineVirtualCamera camera;

    private int EffectorsCount;

    void Start()
    {
        EffectorsCount = effectors.Length;
    }

    private void ShakeCamera ()
    {
        camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1;
    }

    private void ShakePlatforms ()
    {
        for (int i = 0; i < EffectorsCount; i++)
        {
            effectors[i].enabled = !effectors[i].enabled;
        }
    }

    private void OnOffPlatforms ()
    {
        for (int i = 0; i < EffectorsCount; i++)
        {
            effectors[i].gameObject.SetActive(!effectors[i].gameObject.activeSelf);
        }

    }

    public void Shake()
    {
        ShakeCamera();
        ShakePlatforms();
        OnOffPlatforms(); 
        StartCoroutine(Curo());
    }

    private IEnumerator Curo ()
    {
        yield return new WaitForEndOfFrame();
        OnOffPlatforms();
        yield return new WaitForSeconds(0.6f);
        camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
        ShakePlatforms();
    }
}
