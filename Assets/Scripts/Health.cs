using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public bool isAlive;

    [SerializeField] private HealthBarUI healthUI;
    [SerializeField] private float maxHealth;
    [SerializeField] private AudioSource damageAudio;
    [SerializeField] private AudioSource recoveryAudio;

    private float currentHealth;

    private void Awake()
    {
        currentHealth = maxHealth;
        isAlive = true;
    }

    public void TakeDamage (float damage)
    {
        if (damageAudio)
            damageAudio.Play();
        currentHealth -= damage;
        healthUI.UpdateHP(currentHealth, maxHealth);
        CheckIsAlive();
    }

    public void RecoveryHealth (float hitPoints)    
    {
        if (recoveryAudio)
            recoveryAudio.Play();
        currentHealth += hitPoints;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
        healthUI.UpdateHP(currentHealth, maxHealth);
    }

    private void CheckIsAlive ()
    {
        if (currentHealth > 0)
            isAlive = true;
        else
        {
            isAlive = false;
            if (gameObject.GetComponent<Death>())
                gameObject.GetComponent<Death>().GameOver();
            else Destroy(gameObject);
        }
    }
}
