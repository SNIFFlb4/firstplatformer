using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatlhDiller : MonoBehaviour
{
    [SerializeField] private float hitPoints;

    private AudioSource audio;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    //Восстановление здоровья
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            audio.Play();
            collision.gameObject.GetComponent<Health>().RecoveryHealth(hitPoints);
            Destroy(gameObject);
        }
    }
}
