using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
    [Header("MoveSettings")]
    [SerializeField] private float Speed;
    [SerializeField] private float TimeBeforeRun;
    [SerializeField] private float AttackTime;

    [Header("AttackParams")]
    [SerializeField] private Transform attackPosition;
    [SerializeField] private LayerMask PlayerLayer;
    [SerializeField] private float attackDamage;
    [SerializeField] private GameObject[] Particles;
    [SerializeField] private AudioSource attackAudio;

    private Rigidbody2D rigitbody;
    private Animator animator;
    private SpriteRenderer sprite;
    private Health health;

    private AnimState BossState
    {
        get { return (AnimState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }

    private float CurrentTimeToRun;
    private int AttackCounter = 0;

    private void Start()
    {
        rigitbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        health = GetComponent<Health>();

        BossState = AnimState.IDLE;

        CurrentTimeToRun = 0;
    }

    private void Update()
    {
        //Debug.Log($"AnimState = {BossState}");
        if (health.isAlive)
        {
            if (CurrentTimeToRun >= TimeBeforeRun)
            {
                CurrentTimeToRun = 0;
                BossState = AnimState.RUN;
            }

            switch (BossState)
            {
                case AnimState.IDLE:
                    CurrentTimeToRun += Time.deltaTime;
                    break;
                case AnimState.RUN:
                    rigitbody.velocity = Vector2.right * Speed;
                    break;
                case AnimState.REVERT:
                    CurrentTimeToRun += Time.deltaTime;
                    break;
                case AnimState.ATTACK:
                    BossState = AnimState.ATTACK;
                    StartCoroutine(TimeCuro());
                    break;
                default:
                    CurrentTimeToRun = 0;
                    break;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (health.isAlive)
        {
            if (collision.CompareTag("EnemyStopper"))
            {
                sprite.flipX = !sprite.flipX;
                Speed *= -1;

                BossState = AnimState.REVERT;
            }
            if (collision.CompareTag("EnemyAttackPoint"))
            {
                if (AttackCounter == 2)
                {
                    AttackCounter = 0;
                    BossState = AnimState.ATTACK;
                }
                else AttackCounter++;
            }
        }
    }

    private IEnumerator TimeCuro()
    {
        yield return new WaitForSeconds(AttackTime);
        if (health.isAlive)
            BossState = AnimState.RUN;
    }

    public void CloseAttackOn()
    {
        attackAudio.Play();
        for (int i = 0; i < Particles.Length; i++)
            Particles[i].SetActive(true);
        Collider2D[] enemies = Physics2D.OverlapAreaAll(new Vector2(attackPosition.position.x - 2, attackPosition.position.y),
                                                        new Vector2(attackPosition.position.x + 2, attackPosition.position.y),
                                                        PlayerLayer);
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].GetComponent<Health>().TakeDamage(attackDamage);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(attackPosition.position.x - 2, attackPosition.position.y, 0),
                        new Vector3(attackPosition.position.x + 2, attackPosition.position.y, 0));
    }

    public void ExitAnimation()
    {
        BossState = AnimState.EXIT;
    }
}

public enum AnimState
{
    IDLE,
    RUN,
    REVERT,
    ATTACK,
    DEATH,
    EXIT=10
}

