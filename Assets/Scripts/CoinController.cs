using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinController : MonoBehaviour
{
    [SerializeField] private AudioSource coinAudio;
    [SerializeField] private MainCanvas canvas;
    //[SerializeField] private Text ScoreText;

    private int CoinCount=0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
        {
            coinAudio.Play();
            CoinCount+=10;
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("BigCoin"))
        {
            coinAudio.Play();
            CoinCount += 100;
            Destroy(collision.gameObject);
        }

        canvas.UpdateScore(CoinCount);
        //ScoreText.text = CoinCount.ToString();
    }
}
